FROM mongo:4.2.3

WORKDIR .

CMD mongod

EXPOSE 27017
EXPOSE 28017
